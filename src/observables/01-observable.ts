import { Observable, Observer } from 'rxjs';

const observer: Observer<any> = {
    next: value => console.log('siguiente [next]: ', value),
    error: error => console.warn('error [obs]: ', error),
    complete: () => console.info('completado [obs]')
};


// Siempre que sea un observable este debe tener al final un $
// const obs$ = Observable.create(); forma antigua de crear un observable
// const obs% = new Observable<String>(); forma de asignar un valor fijo string, int, etc.
const obs$ = new Observable<string>(subs => {
    subs.next('Hola');
    subs.next('Mundo');

    // forzamos un error para el subscriber
    // const a = undefined;
    // a.nombre = 'Braulio López'

    subs.complete();
});

// obs$.subscribe(console.log);
// obs$.subscribe(resp => {
//      console.log(resp);
//  });

//obs$.subscribe(observer);
obs$.subscribe()

// obs$.subscribe(
//     valor => console.log('next: ', valor),
//     error => console.warn('error: ', error),
//     () => console.info('complete')
// )